#! /bin/sh
set -eu

SO=$HOME/sauteed-onions/monitor
DBDIR=db
DATADIR=data

for log in $HOME/logs/active/*; do
    [ -d "$log" ] && $SO/scripts/reap-onions.sh $DBDIR $(basename "$log") "$log"/$DATADIR
done
