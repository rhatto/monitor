#! /usr/bin/env python3

import sys
import json
import os
from binascii import hexlify, unhexlify
from hashlib import sha256

def writefile(dir, fn, content):
    with open(os.path.join(dir, fn), mode='x') as f:
        f.write(content)

for log in json.load(open(sys.argv[1]))['logs']:
    if log.get('disqualified_at'):
        continue
    dir = hexlify(sha256(log['description'].encode()).digest()).decode()
    if not os.path.exists(dir):
        os.mkdir(dir)
    writefile(dir, 'name.txt', '{}\n'.format(log['description']))
    writefile(dir, 'url.txt', 'https://{}\n'.format(log['url']))
    writefile(dir, 'pubkey', '{}\n'.format(log['key']))
