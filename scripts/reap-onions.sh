#! /usr/bin/env bash
set -eu

# For all cert files (*.pem) in a given diretory, move those with a
# SAN matching ^[a-z0-9]{56}onion\. to the db directory, with the name
# of the log in the new path. Rename all other cert files to .seen, to
# not have to consider them again.

# Update index file mapping all SAN's matching
# ^[a-z0-9]{56}onion\. to onion name and path-to-pemfile, by appending
# to index file (ie not sorting the file).

DBDIR="$1"; shift
LOGNAME="$1"; shift
SRCDIR="$1"; shift

INDEXFILE="$DBDIR/index"

[ -d "${DBDIR}/logs/${LOGNAME}" ] || mkdir -p "${DBDIR}/logs/${LOGNAME}"

AWK='/DNS:/ {
    matches = 0
    split($0, A, /:|,/)
    for (n in A) {
    	if (A[n] ~ /DNS/) {
	    if  (A[++n] ~ /^[a-z0-9]+onion\./ && length(A[n]) >= 56 + 6 + 1) {
	    	printf("%s %s.onion\n", A[n], substr(A[n], 1, 56))
		matches++
	    }
        }
    }
    if (matches == 0)
        print "-", "-"
}'

find $SRCDIR -name \*.pem | while read -r fn; do
    openssl x509 -noout -ext subjectAltName -in $fn | awk "$AWK" | \
	while read -r name onion; do
	    if [ "$name" != "-" ]; then
		newfn="${DBDIR}/logs/${LOGNAME}/$(basename $fn)"
		[ -f $fn ] && mv -n $fn $newfn
		echo $name $onion $newfn >> $INDEXFILE
	    else
		mv -n $fn ${fn}.seen
	    fi
	done
done
