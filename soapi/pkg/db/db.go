package db

import (
	"bytes"
	"context"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"git.sigsum.org/sigsum-go/pkg/log"
)

// The database file is a text file on the format
//   <sauteed onion name> SP <onion name> SP <relative path to PEM> NL
//
// Note that the relative path to the cert file MUST be on the form
//   dir1/dir2/<logname>/<int>.pem
// where <logname> matches a name in 'logs' and <int> is the CT log
// index for the certificate in that log.
//
// See https://www.sauteed-onions.org/db/index for a live database
// file.

type Db struct {
	chReq    chan request
	filename string // configuration
	mtime    time.Time
	db       []record
}

func New(ctx context.Context, fn string) (*Db, error) {
	db := &Db{
		filename: fn,
	}
	go db.run(ctx)
	return db, nil
}

func (d *Db) Get(ctx context.Context, id string) ([]record, error) {
	chResp := d.sendReq(request{cmd: cmdGet, arg: id})
	defer close(chResp)
	select {
	case response := <-chResp:
		var err error
		if response == nil {
			err = fmt.Errorf("not found")
		}
		return response, err
	}
}
func (d *Db) Match(ctx context.Context, name string) ([]record, error) {
	chResp := d.sendReq(request{cmd: cmdMatch, arg: name})
	defer close(chResp)
	select {
	case response := <-chResp:
		return response, nil
	}
}

func (r *record) DomainName() string {
	i := strings.Index(r.Sauteed, ".")
	if i == -1 {
		return ""
	}
	return r.Sauteed[i+1:]
}

func (r *record) LogId() string {
	var logs = []struct {
		name string
		id   string
	}{
		{"00e949280536b5b2d2e7941bc22392f207939d3a91e0f40478d50d69b61b28f5", "s3N3B+GEUPhjhtYFqdwRCUp5LbFnDAuH3PADDnk2pZo="},
		{"02cf3837dce7b1c93d6c6ff1974b08a2887b5872a0177b5435f6a692afc0fa33", "rfe++nz/EMiLnT2cHj4YarRnKV3PsQwkyoWGNOvcgoo="},
		{"088a6445aef866848739e844468dfc180690f3c0a5bf265d020573fdfe4e3d7a", "6H6nZgvCbPYALvVyXT/g4zG5OTu5L79Y6zuQSdr1Q1o="},
		{"5330772bea9d77f5563182552f104dae70fd87c110258849b5ddccaa17905b60", "tz77JN+cTbp18jnFulj0bF38Qs96nzXEnh0JgSXttJk="},
		{"55c6ea0982916817189618ac0ed7a1f074727ae8479ac2ea7907aeb13e231d37", "BZwB0yDgB4QTlYBJjRF8kDJmr69yULWvO0akPhGEDUo="},
		{"640cec3565b81ec9c0944b7ee77ec4db2a199c1e81b7b6d36b883cddd665f9d7", "Nc8ZG7+xbFe/D61MbULLu7YnICZR6j/hKu+oA8M71kw="},
		{"6b1381a9ef130ab2a2178d036a56384a0a12a97f725ee067bcaeed2ca74f35dc", "ejKMVNi3LbYg6jjgUh7phBZwMhOFTTvSK8E6V6NS61I="},
		{"85e0a1cfddc2312c7e90b76d516c1dbaa3a2eefc43dfced984e166bacd104b6b", "w2X5s2VPMoPHnamOk9dBj1ure+MlLJjh0vBLuetCfSM="},
		{"acad5f62ae7b04ebeaa696c118069961fe25d7bb90132798acc17ed340cd5fe8", "UaOw9f0BeZxWbbg3eI8MpHrMGyfL956IQpoN/tSLBeU="},
		{"adae40a2011d0f1c0881f897b0fe12b42ebff9dbce521f0218c59ca7b89fa6d7", "RqVV63X6kSAwtaKJafTzfREsQXS+/Um4havy/HD+bUc="},
		{"b11834071d1e32b685aad20e48bb35785e0ba452ae9df31881ad24fbe27ecc1e", "VYHUwhaQNgFK6gubVzxT8MDkOHhwJQgXL6OqHQcT0ww="},
		{"bc673199dbd7e4a9498779af3dee6ec95dddcbd5ba48460d75defffdf4f0d778", "36Veq2iCTx9sre64X04+WurNohKkal6OOxLAIERcKnM="},
		{"bd32078a464341ed04f4ca1ab95b0b5dc814b8829187322f9d28dd787639885d", "KXm+8J45OSHwVnOfY6V35b5XfZxgCvj5TV0mXCVdx4Q="},
		{"d06bd64582cebc46bfd6c3f1f465483959aba290411c116f644c4ffdc2154efc", "b1N2rDHwMRnYmQCkURX/dxUcEdkCwQApBo2yCJo32RM="},
		{"d29588fffd521fc63028c406e2c87a8608e219c2a00d7ebd46c20d23bd2fb2ec", "QcjKsd8iRkoQxqE6CUKHXk4xixsD6+tLx2jwkGKWBvY="},
		{"d60e8cce32ad2478b552c12eefdf5289bd405049696a6680ae41a09ff7fbb12e", "6D7Q2j71BjUy51covIlryQPTy9ERa+zraeF3fW0GvW4="},
		{"Mammoth", "b1N2rDHwMRnYmQCkURX/dxUcEdkCwQApBo2yCJo32RM="},
		{"Nessie2022", "UaOw9f0BeZxWbbg3eI8MpHrMGyfL956IQpoN/tSLBeU="},
		{"Nimbus2022", "QcjKsd8iRkoQxqE6CUKHXk4xixsD6+tLx2jwkGKWBvY="},
		{"Oak2022", "36Veq2iCTx9sre64X04+WurNohKkal6OOxLAIERcKnM="},
		{"Sabre", "VYHUwhaQNgFK6gubVzxT8MDkOHhwJQgXL6OqHQcT0ww="},
		{"Yeti2022-2", "BZwB0yDgB4QTlYBJjRF8kDJmr69yULWvO0akPhGEDUo="},
	}
	parts := strings.Split(r.CertPath, "/")
	if len(parts) < 3 {
		return ""
	}
	for _, l := range logs {
		if l.name == parts[2] {
			return l.id
		}
	}
	return "" // Failure.
}

func (r *record) LogIndex() uint64 {
	parts := strings.Split(r.CertPath, "/")
	if len(parts) < 4 {
		return 0 // Failure.
	}
	ixStr := strings.TrimSuffix(parts[3], ".pem")
	ix, err := strconv.ParseUint(ixStr, 10, 64)
	if err != nil {
		return 0 // Failure.
	}
	return ix
}

////////////////////
type record struct {
	Index    uint64 // Unique id, stable over invocations
	Sauteed  string // Sauteed onion name, ie <onion>onion.<domain name>
	Onion    string // Onion name, ie <onion>.onion
	CertPath string // Path to local copy of cert, relative
}

type command int

const (
	cmdGet command = iota
	cmdMatch
)

type request struct {
	ch  chan []record
	cmd command
	arg string
}

func (d *Db) run(ctx context.Context) {
	d.chReq = make(chan request, 128)
	defer close(d.chReq)

	ticker := time.NewTicker(1 * time.Second)
	defer ticker.Stop()

	d.readFileMaybe()
	for {
		select {
		case <-ticker.C:
			d.readFileMaybe()
		case req := <-d.chReq:
			req.ch <- d.handleRequest(req)
		case <-ctx.Done():
			return
		}
	}
}

func (d *Db) handleRequest(req request) (result []record) {
	switch req.cmd {
	case cmdGet:
		id, err := strconv.Atoi(req.arg)
		if err != nil {
			log.Debug("dbGet: invalid input: %v", req.arg)
			return nil
		}
		if id < len(d.db) {
			result = append(result, d.db[id])
		}
	case cmdMatch:
		for _, r := range d.db {
			if req.arg == r.DomainName() {
				result = append(result, r)
			}
		}
	}
	return result
}

func (d *Db) sendReq(req request) chan []record {
	req.ch = make(chan []record, 1)
	d.chReq <- req
	return req.ch
}

func (d *Db) readFileMaybe() {
	// Preallocating less than what the unit tests use, to catch
	// errors with growing the array.
	d.db = make([]record, 0, 2) // Fresh db.

	fi, err := os.Stat(d.filename)
	if err != nil {
		log.Warning("stat'ing db file: %v", err)
		return
	}
	if fi.ModTime().Sub(d.mtime) == 0 {
		return
	}
	d.mtime = fi.ModTime()

	// TODO: read one line at a time and skip the splitting on \n
	buf, err := os.ReadFile(d.filename)
	if err != nil {
		log.Warning("reading db file: %v", err)
		return
	}

	for i, line := range bytes.Split(buf, []byte("\n")) {
		parts := strings.Fields(string(line))
		if len(parts) < 3 {
			continue
		}
		d.db = append(d.db, record{
			Index:    uint64(i),
			Sauteed:  string(parts[0]),
			Onion:    string(parts[1]),
			CertPath: string(parts[2]),
		})
	}
}
