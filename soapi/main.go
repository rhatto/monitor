package main

import (
	"context"
	"net/http"

	"git.sigsum.org/sigsum-go/pkg/log"
	"gitlab.torproject.org/tpo/onion-services/sauteed-onions/monitor/soapi/internal/handler"
	"gitlab.torproject.org/tpo/onion-services/sauteed-onions/monitor/soapi/pkg/db"
)

func main() {
	//log.SetLevel(log.InfoLevel)
	log.SetLevel(log.DebugLevel)
	log.SetColor(false)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	db, err := dbSetup(ctx)
	if err != nil {
		log.Fatal("db setup: %v", err)
	}
	server := serverSetup(db)
	if err := server.ListenAndServe(); err != http.ErrServerClosed {
		log.Fatal("HTTP server: %v", err)
	}
}

func dbSetup(ctx context.Context) (*db.Db, error) {
	return db.New(ctx, "index")
}

func serverSetup(db *db.Db) *http.Server {
	mux := http.NewServeMux()
	mux.Handle("/get", handler.Handler{handler.Get, db})
	mux.Handle("/search", handler.Handler{handler.Search, db})
	return &http.Server{Addr: "localhost:8080", Handler: mux}
}
