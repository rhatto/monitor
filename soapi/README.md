soapi is an implementation of an HTTP service for querying a
[sauteed onion](https://www.sauteed-onions.org/) monitor for sauteed
onion certificates.

## Spec

The specification for version 0 (v0) of the API can be found at
https://gitlab.torproject.org/tpo/onion-services/sauteed-onions/monitor/-/blob/main/doc/api.md

## Try it out
```
$ curl -LOs https://www.sauteed-onions.org/db/index
$ go build
$ ./soapi &
[1] 75771
$ curl -s http://localhost:8080/search?in= | json_pp
[
   {
      "domain_name" : "www.sauteed-onions.org",
      "identifiers" : [
         "2",
         "3",
         "24",
         "25",
         "28",
         "29",
         "37"
      ],
      "onion_addr" : "qvrbktnwsztjnbga6yyjbwzsdjw7u5a6vsyzv6hkj75clog4pdvy4cyd.onion"
   }
]
$ curl -s http://localhost:8080/get?id=37 | json_pp
{
   "cert_path" : "db/logs/Oak2022/339159779.pem",
   "domain_name" : "www.sauteed-onions.org",
   "log_id" : "36Veq2iCTx9sre64X04+WurNohKkal6OOxLAIERcKnM=",
   "log_index" : 339159779,
   "onion_addr" : "qvrbktnwsztjnbga6yyjbwzsdjw7u5a6vsyzv6hkj75clog4pdvy4cyd.onion"
}
```
