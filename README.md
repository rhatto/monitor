# Sauteed Onions Monitor

Sauteed onions associate registered domain names with onion addresses.  These
associations are established in CA-issued and CT-logged TLS certificates,
thereby making them publicly enumerable and hard to tamper with.

## Repository structure

This repository contains code and documentation for setting up CT log followers
and a search function for querying the database being built by those followers.

  - [doc/](./doc) - API documentation and setup instructions
  - [follow-go/](./follow-go) - CT monitor that looks for sauteed onions
  - [scripts/](./scripts) - Various scripts to set things up
  - [soapi/](./soapi) - Implementation of the sauteed onion search API

## Public prototype

Available on a best-effort level until at least December 2022:

  - https://api.sauteed-onions.org/
  - http:////zpadxxmoi42k45iifrzuktwqktihf5didbaec3xo4dhvlw2hj54doiqd/

## More information

Refer to [www.sauteed-onions.org][] for quick-start instructions, an FAQ, and
our pre-printed paper.

[www.sauteed-onions.org]: https://www.sauteed-onions.org/
